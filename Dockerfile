FROM node:7.9.0

RUN useradd --user-group --create-home --shell /bin/false app

ENV HOME=/home/app
COPY package.json npm-shrinkwrap.json $HOME/hello/
RUN chown -R app:app $HOME/*

USER app

WORKDIR $HOME/hello
RUN npm install

USER root
COPY ./app/* $HOME/hello
RUN chown -R app:app $HOME/* 
USER app

CMD ["node", "app.js"]
